
# coding: utf-8

# ## Sentiment Classifer Assignment

# In[1]:

import graphlab


# In[7]:

products = graphlab.SFrame('amazon_baby.gl/')


# In[8]:

selected_words = ['awesome', 'great', 'fantastic', 'amazing', 'love', 
                  'horrible', 'bad', 'terrible', 'awful', 'wow', 'hate']


# In[9]:

products.head()


# In[10]:

products['word_count'] = graphlab.text_analytics.count_words(products['review'])


# In[11]:

products.head()


# In[31]:

def word_mentions(word_dict, the_word):
    if the_word in word_dict:
        the_word = word_dict[the_word]
    else:
        the_word = 0
    return the_word


# In[35]:

# test dictionary
# test_dic = {"good": 1, "and" : 34, "bad": 9}

for the_word in selected_words:
    new_column_name = the_word + "_count"
    
    # by using lambda x, we can add another parameter to the function call.
    products[new_column_name] = products['word_count'].apply(lambda x: word_mentions(x, the_word))


# In[34]:

products.head()


# # Discover most commonly used words from these words

# In[45]:

for the_word in selected_words:
    col_name = the_word + "_count"
    print "Total count for word " + the_word +": " + str(products[col_name].sum())


# ## Quiz: Most: Great = 45206  Least: Wow = 144

# # Build a sentiment classifier

# In[46]:

# ignore all 3* reviews
products = products[products['rating'] != 3]
#positive sentiment = 4* or 5* reviews
products['sentiment'] = products['rating'] >=4


# # Predicting the majority class (0.8411233)

# In[118]:


import math
all_rows = products.num_rows()
print all_rows
pos_rows = products[products['sentiment'] == 1].num_rows()
print pos_rows
neg_rows = products[products['sentiment'] == 0].num_rows()
print neg_rows
maj_class = 0.0
maj_class = pos_rows / (all_rows * 1.0)
print str(maj_class)


# ## Check data

# In[47]:

products.head()


# ## Training 

# In[54]:

train_data,test_data = products.random_split(.8, seed=0)


# In[56]:

# Build an array of column names
col_array = []

for the_word in selected_words:
    col_name = the_word + "_count"
    col_array.append(str(col_name))
    
selected_words_model = graphlab.logistic_classifier.create(train_data,
                                                     target='sentiment',
                                                     features=col_array,
                                                     validation_set=test_data)


# In[89]:

selected_words_model['coefficients'].print_rows(num_rows=12)


# ## Quiz: Sort by coefficient values

# In[90]:

selected_words_model['coefficients'].sort('value', ascending=False).print_rows(num_rows=12)


# # Evaluate the sentiment model

# In[65]:

graphlab.canvas.set_target('ipynb')


# In[67]:

selected_words_model.evaluate(test_data)


# In[66]:

selected_words_model.evaluate(test_data, metric='roc_curve')


# ## What is the accuracy of the selected_words_model on the test_data? 0.8431419649291376
# ## What was the accuracy of the sentiment_model that we learned using all the word counts in the IPython Notebook above from the lectures? 0.916256305548883
# ## What is the accuracy majority class classifier on this task? 1/k = 0.2?
# 

# # Focus on 'Baby Trend Diaper Champ' reviews

# In[68]:

diaper_champ_reviews = products[products['name'] == 'Baby Trend Diaper Champ']


# In[69]:

len(diaper_champ_reviews)


# In[70]:

diaper_champ_reviews['rating'].show(view='Categorical')


# # Use selected words model to predict ratings of this product

# In[72]:

diaper_champ_reviews['predicted_sentiment'] = selected_words_model.predict(diaper_champ_reviews, output_type='probability')


# In[73]:

diaper_champ_reviews.head()


# ## Sort the reviews based on the predicted sentiment and explore

# In[74]:

diaper_champ_reviews = diaper_champ_reviews.sort('predicted_sentiment', ascending=False)


# In[124]:

diaper_champ_reviews.head()


# # Train next model
# 

# In[128]:

sentiment_model = graphlab.logistic_classifier.create(train_data,
                                                     target='sentiment',
                                                     features=['word_count'],
                                                     validation_set=test_data)


# ## Add in column for predicted sentiment for the all_words model

# In[131]:

diaper_champ_reviews['predicted sentiment all model'] = sentiment_model.predict(diaper_champ_reviews, output_type='probability')


# In[136]:

diaper_champ_reviews.head()


# ## Find most positive review from all_words column

# In[139]:

diaper_champ_reviews = diaper_champ_reviews.sort('predicted sentiment all model', ascending=False)
diaper_champ_reviews.head()


# # Predicted sentiment for most postive review from all_words model = 0.999999937267

# ## View this review

# In[140]:

diaper_champ_reviews[0]['review']


# ## Run this review with selected words model

# In[143]:

selected_words_model.predict(diaper_champ_reviews[0:1], output_type='probability')


# # Predicted sentiment for most postive review from selected_words model = 0.7969
